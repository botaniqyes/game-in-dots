import { GET_WINNERS, SEND_WINNER } from '~d/actionTypes.js'

let initialState = {
    winners: [],
    winner: {}
};


function winners(state = initialState, action) {
    switch(action.type) {
        case GET_WINNERS:
            return Object.assign({}, state, {
                winners: action.payload
            });

        case SEND_WINNER:
            return Object.assign({}, state, {
                winner: action.payload
            });
            
        default:
            return state;
    }
}

export default winners;