import { combineReducers } from 'redux';
import { RESET_APP } from '~d/actionTypes.js'
import dropdown from './dropdown.js';
import menu from './menu.js';
import winners from './winners.js';
import board from './board.js';
import message from './message.js';

/* Unite all reducers */
const appReducer = combineReducers({
    dropdown,
    menu,
    winners,
    board,
    message
});
  
const rootReducer = (state, action) => {
    if (action.type === RESET_APP) {
      //state = undefined;
      state.board = undefined;
    }
  
    return appReducer(state, action);
};

export default rootReducer;