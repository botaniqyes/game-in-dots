import { UPDATE_VALUE, CHANGE_BUTTON_CAPTION, CHANGE_BUTTON_DISABLED } from '~d/actionTypes.js'

let initialState = {
    value: 'Anonymous',
    buttonCaption: 'Play',
    buttonDisabled: true
};


function menu(state = initialState, action) {
    switch(action.type) {
        case UPDATE_VALUE:
            return Object.assign({}, state, {
                value: action.payload
            });

        case CHANGE_BUTTON_CAPTION:
            return Object.assign({}, state, {
                buttonCaption: action.payload
            });
            
        case CHANGE_BUTTON_DISABLED:
            return Object.assign({}, state, {
                buttonDisabled: action.payload
            });

        default:
            return state;
    }
}

export default menu;