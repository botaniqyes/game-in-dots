import { UPDATE_RANDOM_FIELD, ADD_GREEN_FIELD, ADD_RED_FIELD } from '~d/actionTypes.js';

let initialState = {
    randomField: {},
    greenFields: [],
    redFields: [] 
};

function board(state = initialState, action) {
    switch(action.type) {
        case UPDATE_RANDOM_FIELD:
            return Object.assign({}, state, {
                randomField: action.payload
            });

        case ADD_GREEN_FIELD:
            return Object.assign({}, state, {
                greenFields: [...state.greenFields, action.payload]
            });

        case ADD_RED_FIELD:
            return Object.assign({}, state, {
                redFields: [...state.redFields, action.payload]
            });
            
        default:
            return state;
    }
}

export default board;