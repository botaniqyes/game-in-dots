import { GET_MODES, ACTIVATE_MODE, CHANGE_DROPDOWN_TITLE, CHANGE_DROPDOWN_DISABLED } from '~d/actionTypes.js'

let initialState = {
    modes: [],
    activeMode: {},
    title: 'Pick game mode',
    disabled: false
};

function dropdown(state = initialState, action) {
    switch(action.type) {
        case GET_MODES:
            let modesList = [];
            let modesObj = action.payload;

            for (let key in modesObj) {
                modesList.push({
                    id: modesList.length,
                    name: key[0].toUpperCase() + key.slice(1, -4),
                    field: modesObj[key].field,
                    delay: modesObj[key].delay,
                    active: false,
                    disabled: false
                })
            }

            return Object.assign({}, state, {
                modes: modesList 
            });

        case ACTIVATE_MODE:
            let id = action.payload;
            let modeNamesCopy = [...state.modes];

            let modes = modeNamesCopy.map( (el, i) => {
                if (id === i.toString()) {
                    return modeNamesCopy[i] = {...modeNamesCopy[i], active: true, disabled: true};
                }
    
                return modeNamesCopy[i] = {...modeNamesCopy[i], active: false, disabled: false};
            })

            return Object.assign({}, state, {modes}, {
                activeMode: {
                    field: modes[id].field,
                    delay: modes[id].delay
                }
            });

        case CHANGE_DROPDOWN_TITLE:
            return Object.assign({}, state, {
                title: action.payload
            });
        
        case CHANGE_DROPDOWN_DISABLED:
            return Object.assign({}, state, {
                disabled: action.payload
            });

        default:
            return state;
    }
}

export default dropdown;