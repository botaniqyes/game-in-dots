import { UPDATE_MESSAGE } from '~d/actionTypes.js';

let initialState = {
    text: ''
};


function message(state = initialState, action) {
    switch(action.type) {
        case UPDATE_MESSAGE:
            return Object.assign({}, state, {
                text: action.payload
            });

        default:
            return state;
    }
}

export default message;