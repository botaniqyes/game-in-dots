export const RESET_APP = 'RESET_APP';

/* dropdown */
export const GET_MODES = 'GET_MODES';
export const ACTIVATE_MODE = 'ACTIVATE_MODE';
export const CHANGE_DROPDOWN_TITLE = 'CHANGE_DROPDOWN_TITLE';
export const CHANGE_DROPDOWN_DISABLED = 'CHANGE_DROPDOWN_DISABLED';

/* menu */
export const UPDATE_VALUE = 'UPDATE_VALUE';
export const CHANGE_BUTTON_CAPTION = 'CHANGE_BUTTON_CAPTION';
export const CHANGE_BUTTON_DISABLED = 'CHANGE_BUTTON_DISABLED';

/* winners */
export const GET_WINNERS = 'GET_WINNERS';
export const SEND_WINNER = 'SEND_WINNER';

/* board */
export const UPDATE_RANDOM_FIELD = 'UPDATE_RANDOM_FIELD';
export const ADD_GREEN_FIELD = 'ADD_GREEN_FIELD';
export const ADD_RED_FIELD = 'ADD_RED_FIELD';

/* message */
export const UPDATE_MESSAGE = 'UPDATE_MESSAGE';
