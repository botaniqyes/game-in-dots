import { GET_WINNERS, SEND_WINNER } from '~d/actionTypes.js'

export const getWinners = (data) => dispatch => {
    dispatch({ type: GET_WINNERS, payload: data});
}

export const sendWinner = (data) => dispatch => {
    dispatch({ type: SEND_WINNER, payload: data});
}