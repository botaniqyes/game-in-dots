import { GET_MODES, ACTIVATE_MODE, CHANGE_DROPDOWN_TITLE, CHANGE_DROPDOWN_DISABLED } from '~d/actionTypes.js'

export const getModes = (data) => dispatch => {
    dispatch({ type: GET_MODES, payload: data});
}

export const activateMode = (data) => dispatch => {
    dispatch({ type: ACTIVATE_MODE, payload: data});
}

export const changeDropdownTitle = (data) => dispatch => {
    dispatch({ type: CHANGE_DROPDOWN_TITLE, payload: data});
}

export const changeDropdownDisabled = (data) => dispatch => {
    dispatch({ type: CHANGE_DROPDOWN_DISABLED, payload: data});
}