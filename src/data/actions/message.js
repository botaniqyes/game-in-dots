import { UPDATE_MESSAGE } from '~d/actionTypes.js';

export const updateMessage = (data) => dispatch => {
    dispatch({ type: UPDATE_MESSAGE, payload: data});
}