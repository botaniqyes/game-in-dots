import { UPDATE_RANDOM_FIELD, ADD_GREEN_FIELD, ADD_RED_FIELD } from '~d/actionTypes.js';

export const updateRandomField = (data) => dispatch => {
    dispatch({ type: UPDATE_RANDOM_FIELD, payload: data});
}

export const addGreenField = (data) => dispatch => {
    dispatch({ type: ADD_GREEN_FIELD, payload: data});
}

export const addRedField = (data) => dispatch => {
    dispatch({ type: ADD_RED_FIELD, payload: data});
}