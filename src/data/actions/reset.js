import { RESET_APP } from '~d/actionTypes.js'

export const resetApp = () => dispatch => {
    dispatch({ type: RESET_APP});
}
