import { UPDATE_VALUE, CHANGE_BUTTON_CAPTION, CHANGE_BUTTON_DISABLED } from '~d/actionTypes.js'

export const updateValue = (data) => dispatch => {
    dispatch({ type: UPDATE_VALUE, payload: data});
}

export const changeButtonCaption = (data) => dispatch => {
    dispatch({ type: CHANGE_BUTTON_CAPTION, payload: data});
}

export const changeButtonDisabled = (data) => dispatch => {
    dispatch({ type: CHANGE_BUTTON_DISABLED, payload: data});
}