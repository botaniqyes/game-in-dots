import React, { Component } from 'react';
import { connect } from 'react-redux';

import Menu from '~c/menu/menu.js';
import Winners from '~c/winners/winners.js';
import Board from '~c/board/board.js';
import Message from '~c/message/message.js';

import { getModes, activateMode, changeDropdownTitle, changeDropdownDisabled } from '~a/dropdown.js';
import { updateValue, changeButtonCaption, changeButtonDisabled } from '~a/menu.js';
import { getWinners, sendWinner } from '~a/winners.js';
import { updateRandomField, addGreenField, addRedField } from '~a/board.js';
import { updateMessage } from '~a/message.js';
import { resetApp } from '~a/reset.js';

class App extends Component {
    static intervalId = 0;
    static randomFieldStatic;

    /* API */
    fetchGameSettings = () => {
        let query = 'https://starnavi-frontend-test-task.herokuapp.com/game-settings';

        fetch(query)
        .then(res => res.json())
        .then(
            (res) => {
                // console.log(res);
                this.props.getModes(res);
            },
            (error) => {
                console.log(error);
            }
        );
    }

    fetchWinners = () => {
        let query = 'https://starnavi-frontend-test-task.herokuapp.com/winners';

        fetch(query)
        .then(res => res.json())
        .then(
            (res) => {
                // console.log(res);
                this.props.getWinners(res);
            },
            (error) => {
                console.log(error);
            }
        );
    }

    getDate() {
        const monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"];
        let d = new Date();
        let hours = d.getHours().toString();
        let minutes = d.getMinutes().toString();

        function addZeroToDate(time) {
            if (time.length < 2) {
                time = `0${time}`;
            }

            return time;
        }

        hours = addZeroToDate(hours);
        minutes = addZeroToDate(minutes);

        let date = `${hours}:${minutes}; ${d.getDate()} ${monthNames[d.getMonth()]} ${d.getFullYear()}`;

        return date;
    }

    sendResultGame(winner) {
        let query = 'https://starnavi-frontend-test-task.herokuapp.com/winners';
        let date = this.getDate();
        
        fetch(query, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify({winner, date})
        })
        .then(res => res.json())
        .then(
            (res) => {
                // console.log(res);
                this.props.getWinners(res);
            },
            (error) => {
                console.log(error);
            }
        );
        
    }

    componentDidMount() {
        this.fetchGameSettings();
        this.fetchWinners();
    }

    /* board */
    getRandomNumber(min, max) {
        return Math.floor(min + Math.random() * (max + 1 - min));
    }

    getRandomNumbers() {
        let min = 0;
        let max = this.props.store.dropdown.activeMode.field - 1;

        let col = this.getRandomNumber(min, max);
        let row = this.getRandomNumber(min, max);
        return {col, row};
    }

    equalFields(randomField, occupiedFields) {
        for (let i = 0; i < occupiedFields.length; i++) {
            if (randomField.col === occupiedFields[i].col && randomField.row === occupiedFields[i].row) {
                return false;
            }
        }

        return true;
    }

    checkUniqueRandomField(randomField) {
        let {redFields, greenFields} = this.props.store.board;
        return this.equalFields(randomField, greenFields) && this.equalFields(randomField, redFields);
    }

    updateRandomField() {
        let randomField = this.getRandomNumbers();
        let isUniqueRandomField = this.checkUniqueRandomField(randomField);

        while (!isUniqueRandomField) {
            randomField = this.getRandomNumbers();
            isUniqueRandomField = this.checkUniqueRandomField(randomField);
        }

        this.props.updateRandomField(randomField);
        this.randomFieldStatic = randomField;
    }


    startInterval() {
        let {delay} = this.props.store.dropdown.activeMode;
        
        this.intervalId = setInterval(() => {
            this.props.addRedField(this.randomFieldStatic);
            
            let isGameOver = this.checkWinner();
            if (!isGameOver) {
                this.updateRandomField();
            }
        }, delay);  
    }

    checkWinnerFields(fields) {
        let {field} = this.props.store.dropdown.activeMode;
        let halfSquares = (field * field) / 2;

        if (fields.length > halfSquares) {
            clearInterval(this.intervalId);
            this.props.updateRandomField({});

            this.props.changeButtonCaption('Play again');
            this.props.changeDropdownDisabled(false);
            this.props.changeButtonDisabled(false);

            return true;
        }
        
        return false;
    }

    checkWinner() {
        let {redFields, greenFields} = this.props.store.board;

        let isComputerWinner = this.checkWinnerFields(redFields);
        if (isComputerWinner) {
            this.props.updateMessage('Computer won');
            this.sendResultGame('Computer');
            return true;
        }
        
        let isUserWinner = this.checkWinnerFields(greenFields);
        if (isUserWinner) {
            let {value} = this.props.store.menu;
            this.props.updateMessage(`${value} won`);
            this.sendResultGame(`${value}`);
            return true;
        }

        return false;
    }

    handleClickOnField = () => {
        clearInterval(this.intervalId);

        this.props.addGreenField(this.randomFieldStatic);
        this.props.updateRandomField({});
        
        let isGameOver = this.checkWinner();
        if (!isGameOver) {
            this.updateRandomField();
            this.startInterval();
        }
    }

    /* menu */
    handleClickOnPlay = () => {
        this.props.updateMessage('');

        this.props.resetApp();

        this.updateRandomField();
        this.startInterval();

        this.props.changeDropdownDisabled(true);
        this.props.changeButtonDisabled(true);
    }

    render() {
        return (
            <div className="container app">
                <div>
                    <Menu {...this.props} onClick={this.handleClickOnPlay}/>
                    <Message {...this.props} />
                    <Board {...this.props} onClick={this.handleClickOnField}/>
                </div>
                <div>
                    <Winners {...this.props} />
                </div>
            </div>
        );
    }
}

export default connect(
    state => ({
      store: state
    }),
    dispatch => ({
        /* dropdown */
        getModes: (modes) => {
            dispatch( getModes(modes) )
        },
        activateMode: (id) => {
            dispatch( activateMode(id) )
        },
        changeDropdownTitle: (newTitle) => {
            dispatch( changeDropdownTitle(newTitle) )
        },
        changeDropdownDisabled: (data) => {
            dispatch( changeDropdownDisabled(data) )
        },
        /* menu */
        onChange: (value) => {
            dispatch( updateValue(value) )
        },
        changeButtonCaption: (newCaption) => {
            dispatch( changeButtonCaption(newCaption) )
        },
        changeButtonDisabled: (data) => {
            dispatch( changeButtonDisabled(data) )
        },
        /* winners */
        getWinners: (winners) => {
            dispatch( getWinners(winners) )
        },
        sendWinner: (winner) => {
            dispatch( sendWinner(winner) )
        },
        /* board */
        updateRandomField: (field) => {
            dispatch( updateRandomField(field) )
        },
        addGreenField: (field) => {
            dispatch( addGreenField(field) )
        },
        addRedField: (field) => {
            dispatch( addRedField(field) )
        },
        /* message */
        updateMessage: (text) => {
            dispatch( updateMessage(text) )
        },
        /* reset */
        resetApp: () => {
            dispatch( resetApp() )
        }
    })
)(App);