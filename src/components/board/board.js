import React, { Component } from 'react';
import classNames from 'classnames';
import styles from './board.module.css';

class Board extends Component {

    render() {
        let {field} = this.props.store.dropdown.activeMode;
        let squares = [];
        let {randomField, greenFields, redFields} = this.props.store.board;

        function createTd(id, color = '', onClick = () => {}) {
            return (<td className={classNames(styles.field, color)} key={id} onClick={onClick}></td>);
        }

        function createMarkedFields(fileds, rows, i, j, color) {
            if (fileds.length > 0) {
                for (let k = 0; k < fileds.length; k++) {
                    if (fileds[k].row === i && fileds[k].col ===j) {
                        rows.push( createTd(j, color) );
                        return true;
                    } 
                }
            } 
        }

        for (let i = 0; i < field; i++) {
            let rows = [];

            for (let j = 0; j < field; j++) {
                if (randomField.row === i && randomField.col ===j) {
                    rows.push( createTd(j, styles.field_gray, this.props.onClick) );
                    continue;
                }

                let isRedFieled = createMarkedFields(redFields, rows, i, j, styles.field_red);
                if (isRedFieled) {
                    continue;
                }

                let isGreenField = createMarkedFields(greenFields, rows, i, j, styles.field_green);
                if (isGreenField) {
                    continue;
                } 

                rows.push( createTd(j) ); 
            }

            squares.push(
                <tr key={i} style={{display: 'block'}}>
                    {rows}
                </tr>
            )
        }

        return (
            <div className={styles.board}>
                <table className={styles.table}>
                    <tbody>
                        {squares}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default Board;