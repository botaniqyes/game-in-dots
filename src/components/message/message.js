import React, { Component } from 'react';
import styles from './message.module.css';

class Message extends Component {

    render() {
        let {text} = this.props.store.message;
        
        return (
            <div className={styles.message}>
               {text}
            </div>
        );
    }
}

export default Message;