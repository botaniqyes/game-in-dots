import React from 'react';
import PropTypes from 'prop-types';

export default class extends React.Component {
    static defaultProps = {
        value: '',
        onChange: function(e){},
        nativeProps: {},
    }

    static propTypes = {
        value: PropTypes.any,
        onChange: PropTypes.func,
        nativeProps: PropTypes.object 
    }

    nativeInput = React.createRef();

    setValue = (value) => {
        this.nativeInput.current.value = value;
    }

    /* when props changed */
    /*
    componentDidUpdate(prevProps, prevState) {
        let input = this.nativeInput.current;

        if (prevProps.value !== this.props.value ||  input.value != prevProps.value ) {
            input.value = this.props.value;
        }
    }
    */

    checkChange = (e) => {
        if (this.props.value.toString() !== e.target.value) {
            this.props.onChange(e);
        }
    }
    
    checkEnterKey = (e) => {
        if (e.keyCode === 13) {
            this.checkChange(e);
        }
    }

    render() {
        return (
            <>
                <input {...this.props.nativeProps}
                    placeholder="Enter your name"
                    defaultValue={this.props.value} 
                    onBlur={this.checkChange}
                    onKeyUp={this.checkEnterKey}
                    ref={this.nativeInput} 
                />
            </>
        );
    }
}