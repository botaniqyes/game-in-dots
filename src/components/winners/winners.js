import React, { Component } from 'react';
import ListGroup from 'react-bootstrap/ListGroup';
import styles from './winners.module.css';

class Winners extends Component {

    render() {
        let winners = this.props.store.winners.winners;

        let listItems = winners.map( (el, i) => {
            return (
                <ListGroup.Item className={styles.winners__item} variant="secondary" key={winners[i].id}>
                    <div>
                        {winners[i].winner}
                    </div>
                    <div>
                        {winners[i].date}
                    </div>
                </ListGroup.Item>
            );
        });

        let list = (
            <ListGroup>
                {listItems.reverse()}
            </ListGroup>
        );

        return (
            <div className={styles.winners}>
                <h2 className={styles.title}>Leader Board</h2>
                {list}
            </div>
        );
    }
}

export default Winners;