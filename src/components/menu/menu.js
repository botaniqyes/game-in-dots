import React, { Component } from 'react';
import DropDown from '~c/dropdown/dropdown.js';
import AppInput from '~c/input/input.js';

import Button from 'react-bootstrap/Button'
import styles from './menu.module.css';

class Menu extends Component {

    onChange = (e) => {
        let value = e.target.value;
        this.props.onChange(value);
    }

    render() {
        let {buttonCaption, buttonDisabled} = this.props.store.menu;
        
        return (
            <div className={styles.menu}>
                <DropDown {...this.props} />
                <AppInput 
                    nativeProps={{className: styles.menu__input}}
                    onChange={this.onChange}
                />
                <Button className={styles.button} onClick={this.props.onClick} variant="secondary" disabled={buttonDisabled}>
                    {buttonCaption}
                </Button>
            </div>
        );
    }
}

export default Menu;