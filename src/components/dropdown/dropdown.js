import React, { Component } from 'react';
import Dropdown from 'react-bootstrap/Dropdown';

class DropDown extends Component {

    onClick = (e) => {
        let id = e.target.getAttribute('data-id');
        this.props.activateMode(id);
    }

    onSelect = () => {
        this.props.changeButtonDisabled(false);
        this.props.resetApp();
    }

    render() {
        let {title, disabled} = this.props.store.dropdown;
        let {modes} = this.props.store.dropdown;

        let items = modes.map( (el, i) => {
            return (
                <Dropdown.Item
                    key={modes[i].id}
                    data-id={i} 
                    as="div"
                    active={modes[i].active}
                    disabled={modes[i].disabled} 
                    onClick={this.onClick} 
                    onSelect={this.onSelect}>
                    {modes[i].name}
                </Dropdown.Item>
            ); 
        } )

        return (
            <div>
                <Dropdown>
                    <Dropdown.Toggle variant="secondary" id="dropdown-basic" disabled={disabled}>
                        {title}
                    </Dropdown.Toggle> 
                    <Dropdown.Menu>
                        {items}
                    </Dropdown.Menu>
                </Dropdown>
            </div>
        );
    }
}

export default DropDown;